FROM openjdk:17-alpine
COPY uuid-manager-jar-with-dependencies.jar /app/
WORKDIR /app
CMD ["java", "-jar", "uuid-manager-jar-with-dependencies.jar"]