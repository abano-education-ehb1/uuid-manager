package be.ehb.uuidmanager.config.settings.rabbitmq;

import lombok.Data;

@Data
public class RabbitMQCredentialsConfigPart {
    String host = "", username = "", password = "";
    int port = 5672;
}
