package be.ehb.uuidmanager.config.settings.sql;

import lombok.Data;

@Data
public class SqlConfigPart {

    String host = "", database = "", username = "", password = "";
    int port = 3306;
}
