package be.ehb.uuidmanager.config;

import be.ehb.uuidmanager.config.settings.rabbitmq.RabbitMQCredentialsConfigPart;
import be.ehb.uuidmanager.config.settings.sql.SqlConfigPart;
import be.ehb.uuidmanager.utils.config.annotation.Configuration;
import lombok.Data;
import lombok.ToString;

@Data
@Configuration(value = "settings")
@ToString
public class SettingsConfig {

    RabbitMQCredentialsConfigPart rabbitmq = new RabbitMQCredentialsConfigPart();
    SqlConfigPart sql = new SqlConfigPart();
}
