package be.ehb.uuidmanager.utils.config;


import be.ehb.uuidmanager.utils.config.annotation.Configuration;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.val;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class Config {

    /**
     * Read or create a config file on disk from a class structure
     *
     * @param folderName Plugin name
     * @param structure  Class of config structure
     * @return Returns the config file instance
     */
    @SneakyThrows
    public <T> T readOrCreateConfiguration(@NonNull String folderName, @NonNull Class<T> structure) {
        if (!structure.isAnnotationPresent(Configuration.class))
            throw new RuntimeException("Tried to load a non-annotated configuration");

        var configName = structure.getAnnotation(Configuration.class).value();
        if (configName.isEmpty())
            configName = structure.getSimpleName();
        // On veut du YML
        if (!configName.endsWith(".yml"))
            configName = configName + ".yml";

        return readOrCreateConfiguration(folderName, structure, configName);
    }

    /**
     * Read or create a config file with multiple models on disk from a class structure
     *
     * @param folderName Plugin name
     * @param structure  Class of config structure
     * @param models     Enum of models
     * @return Returns a map with keys as model and value as the config file's instance
     */
    public <X extends Enum<?>, Y> Map<X, Y> readOrCreateConfiguration(@NonNull String folderName, @NonNull Class<Y> structure, @NonNull Class<X> models) {
        Map<X, Y> map = new HashMap<>();

        if (!structure.isAnnotationPresent(Configuration.class))
            throw new RuntimeException("Tried to load a non-annotated configuration");

        var configName = structure.getAnnotation(Configuration.class).value();
        if (configName.isEmpty())
            configName = structure.getSimpleName();

        if (configName.endsWith(".yml"))
            configName = configName.replace(".yml", "");

        if (models.getEnumConstants().length == 0)
            throw new RuntimeException("Tried to load a configuration with an empty enum");

        for (X value : models.getEnumConstants()) {
            map.put(value, readOrCreateConfiguration(folderName + "/" + configName, structure, configName + "_" + value.toString().toLowerCase() + ".yml"));
        }

        return map;
    }

    /**
     * Read or create a config file with a specific model on disk from a class structure
     *
     * @param folderName Plugin name
     * @param structure  Class of config structure
     * @param model      The specified model
     * @return Returns the config file instance
     */
    public <X extends Enum<?>, Y> Y readOrCreateConfiguration(@NonNull String folderName, @NonNull Class<Y> structure, @NonNull X model) {
        if (!structure.isAnnotationPresent(Configuration.class))
            throw new RuntimeException("Tried to load a non-annotated configuration");

        var configName = structure.getAnnotation(Configuration.class).value();
        if (configName.isEmpty())
            configName = structure.getSimpleName();

        if (configName.endsWith(".yml"))
            configName = configName.replace(".yml", "");

        return readOrCreateConfiguration(folderName + "/" + configName, structure, configName + "_" + model.toString().toLowerCase() + ".yml");
    }

    /**
     * Read or create the config file on the disk
     *
     * @param folderName Plugin name
     * @param structure  Class of config structure
     * @param configName The name of the config file
     * @return Returns the config file instance
     */
    @SneakyThrows
    private <T> T readOrCreateConfiguration(@NonNull String folderName, @NonNull Class<T> structure, @NonNull String configName) {
        val pluginPath = Paths.get(folderName, configName);
        final T instance;
        if (!Files.exists(pluginPath)) {
            instance = structure.getDeclaredConstructor().newInstance();
            Files.createDirectories(pluginPath.getParent());
            Files.createFile(pluginPath);
        } else {
            instance = Serializations.deserialize(
                    new String(Files.readAllBytes(pluginPath), StandardCharsets.UTF_8),
                    structure
            );
        }

        Files.write(
                pluginPath,
                Serializations.serialize(instance).getBytes(StandardCharsets.UTF_8)
        );
        return instance;
    }

    /**
     * Writes down the config file on the disk
     *
     * @param folderName    Plugin name
     * @param configuration The config instance to write
     */
    @SneakyThrows
    public <T> void writeConfiguration(@NonNull String folderName, @NonNull T configuration) {
        val structure = configuration.getClass();
        if (!structure.isAnnotationPresent(Configuration.class))
            throw new RuntimeException("Tried to load a non-annotated configuration");

        var configName = structure.getAnnotation(Configuration.class).value();
        if (configName.isEmpty())
            configName = structure.getSimpleName();
        // On veut du YML
        if (!configName.endsWith(".yml"))
            configName = configName + ".yml";

        writeConfiguration(folderName, configuration, configName);
    }

    /**
     * Writes down the config files on the disk
     *
     * @param folderName Plugin name
     * @param map        The map that contains the models as keys and config file instances as value
     */
    @SneakyThrows
    public <X extends Enum<?>, Y> void writeConfiguration(@NonNull String folderName, @NonNull Map<X, Y> map) {
        map.keySet().forEach(value -> writeConfiguration(folderName, map, value));
    }

    /**
     * Writes down the config file on the disk
     *
     * @param folderName Plugin name
     * @param map        The map that contains the models as keys and config file instances as value
     * @param model      The specific model to write
     */
    @SneakyThrows
    public <X extends Enum<?>, Y> void writeConfiguration(@NonNull String folderName, @NonNull Map<X, Y> map, @NonNull X model) {
        val structure = map.get(model).getClass();
        if (!structure.isAnnotationPresent(Configuration.class))
            throw new RuntimeException("Tried to load a non-annotated configuration");

        var configName = structure.getAnnotation(Configuration.class).value();
        if (configName.isEmpty())
            configName = structure.getSimpleName();
        // On veut du YML
        if (configName.endsWith(".yml"))
            configName = configName.replace(".yml", "");

        writeConfiguration(folderName + "/" + configName, map.get(model), configName + "_" + model.toString().toLowerCase() + ".yml");
    }

    /**
     * Writes down the config file on the disk
     *
     * @param folderName    Plugin name
     * @param configuration The config instance to write
     * @param configName    The config file name
     */
    @SneakyThrows
    private <T> void writeConfiguration(@NonNull String folderName, @NonNull T configuration, @NonNull String configName) {
        val pluginPath = Paths.get(folderName, configName);
        if (!Files.exists(pluginPath)) {
            Files.createDirectories(pluginPath.getParent());
            Files.createFile(pluginPath);
        }
        Files.write(
                pluginPath,
                Serializations.serialize(configuration).getBytes(StandardCharsets.UTF_8)
        );
    }
}
