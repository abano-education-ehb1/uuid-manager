package be.ehb.uuidmanager.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.val;

@UtilityClass
public class ArgsUtils {

    public boolean isNumeric(@NonNull String string){
        try {
            Integer.parseInt(string);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public String convert(@NonNull String text){
        val sb = new StringBuilder();
        val chars = text.toCharArray();
        for(int i = 0; i < chars.length; i++){
            val c = chars[i];
            if(Character.isUpperCase(c) && i > 0){
                sb.append("-");
            }
            sb.append(Character.toLowerCase(c));
        }
        return sb.toString();
    }
}
