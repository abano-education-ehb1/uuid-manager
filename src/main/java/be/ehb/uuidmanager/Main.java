package be.ehb.uuidmanager;

import be.ehb.uuidmanager.config.SettingsConfig;
import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.enums.Entities;
import be.ehb.uuidmanager.rabbitmq.RabbitMQManager;
import be.ehb.uuidmanager.rabbitmq.listeners.MessageListener;
import be.ehb.uuidmanager.tasks.HeartbeatTask;
import be.ehb.uuidmanager.utils.config.Config;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Timer;

@Log4j2
public class Main {

    @Getter
    private static SettingsConfig settings;

    /**
     * Start function of the app
     *
     * @param args Application arguments
     */
    public static void main(String[] args) {
        try {
            init(args);
        } catch (Exception e) {
            log.error("Application encountered an error:", e);
        }
    }

    public static void init(@NonNull String[] args) {
        // Config files
        settings = Config.readOrCreateConfiguration("configs", SettingsConfig.class);

        log.info("Initialising managers...");
        // Init managers
        DataManager.init(settings.getSql());
        RabbitMQManager.init(settings.getRabbitmq());

        log.info("Listening to queue \"uuid_manager\"");
        // Listen to RabbitMQ messages
        RabbitMQManager.addListener("uuid_manager", new MessageListener());

        // Set up heartbeat task
        new Timer().scheduleAtFixedRate(new HeartbeatTask(), 0, 1000 * 3);
    }
}
