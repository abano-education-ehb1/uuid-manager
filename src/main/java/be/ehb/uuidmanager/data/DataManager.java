package be.ehb.uuidmanager.data;

import be.ehb.uuidmanager.config.settings.sql.SqlConfigPart;
import be.ehb.uuidmanager.data.tables.AbstractEntities;
import be.ehb.uuidmanager.data.tables.Models;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.requery.sql.EntityDataStore;
import io.requery.sql.SchemaModifier;
import io.requery.sql.TableCreationMode;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.val;

@UtilityClass
public class DataManager {

    @Getter
    private static HikariDataSource hikari;

    @Getter
    private EntityDataStore<AbstractEntities> entityUUIDStore;


    public void init(@NonNull SqlConfigPart config){
        // Converting SqConfigPart to a HikariConfig
        val hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:mysql://" + config.getHost() + ":" + config.getPort() + "/" + config.getDatabase());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        hikariConfig.setMaximumPoolSize(5);

        // Init connection
        hikari = new HikariDataSource(hikariConfig);

        // Define EntityDataStores
        entityUUIDStore = new EntityDataStore<>(hikari, Models.DEFAULT);

        // Create tables if not exists
        new SchemaModifier(hikari, Models.DEFAULT).createTables(TableCreationMode.CREATE_NOT_EXISTS);
    }

}
