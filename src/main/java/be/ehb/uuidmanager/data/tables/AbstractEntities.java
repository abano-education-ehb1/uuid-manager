package be.ehb.uuidmanager.data.tables;

import io.requery.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Table(name = "entities")
@Entity
public abstract class AbstractEntities {

    @Id
    @Generated
    int id;

    @Column(length = 36, nullable = false)
    @Setter
    @Getter
    String uuid;

    @Column(length = 30, nullable = false)
    String system;

    @Column(name = "entity_id", nullable = false)
    String entityId;

    @Column(name = "entity_version", value = "1")
    int entityVersion;

    String entityType;

    @Column(name = "created_at", value = "CURRENT_TIMESTAMP")
    Timestamp createdAt;

    @Column(name = "updated_at", value = "CURRENT_TIMESTAMP")
    @Setter
    Timestamp updatedAt;

    @PreInsert
    public void onPreInsert() {
        if (Objects.isNull(getUuid()))
            setUuid(UUID.randomUUID().toString());
    }

    @PreUpdate
    public void onPreUpdate() {
        setUpdatedAt(new Timestamp(new Date().getTime()));
    }
}
