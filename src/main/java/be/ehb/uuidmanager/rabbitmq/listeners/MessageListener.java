package be.ehb.uuidmanager.rabbitmq.listeners;

import be.ehb.uuidmanager.enums.Entities;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.model.Redirection;
import be.ehb.uuidmanager.model.error.Error;
import be.ehb.uuidmanager.rabbitmq.Consumer;
import be.ehb.uuidmanager.rabbitmq.RabbitMQManager;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import lombok.extern.log4j.Log4j2;
import lombok.val;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
public class MessageListener implements Consumer {

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
        System.out.print("\n");
        val xml = new String(body);
        val entity = Entities.convertToEntity(xml);

        // Check if entity is not valid
        if (Objects.isNull(entity)) {
            new Error(Error.Level.ERROR, "An xml has been sent but does not correspond to any xsd.").send();
            log.error("Not valid to any xsd");
            log.error(xml);
            return;
        }

        val model = entity.convert(xml);

        if (Objects.isNull(model)) {
            new Error(Error.Level.ERROR, "Unable to convert the xml to an object.\n" + xml).send();
            log.error("Enable to convert xml to object");
            log.error(xml);
            return;
        }

        log.info(model.retrieveInfo());

        try {
            model.apply();
        } catch (RuntimeException e) {
            new Error(Error.Level.WARNING, e.getMessage()).send();
            log.warn(e.getMessage());
            return;
        }

        val redirectedTo = new ArrayList<Services>();

        entity.getRedirections().stream()
                .filter(redirection -> !redirection.getService().getName().equalsIgnoreCase(model.getSource()))
                .forEach(redirection -> {
                    Model<?> newModel;
                    try {
                        newModel = model.convertToService(redirection.getService());
                    } catch (RuntimeException e) {
                        new Error(Error.Level.WARNING, e.getMessage()).send();
                        log.warn(e.getMessage());
                        return;
                    }
                    if (Objects.isNull(newModel)) {
                        log.warn("Model is null");
                        return;
                    }

                    try {
                        //System.out.println(new XmlMapper().writeValueAsString(newModel));
                        RabbitMQManager.sendMessage(redirection.getQueueName(), new XmlMapper().writeValueAsString(newModel));
                        redirectedTo.add(redirection.getService());
                    } catch (JsonProcessingException e) {
                        new Error(Error.Level.WARNING, e.getMessage()).send();
                        log.error(e.getMessage());
                    }
                });

        if (redirectedTo.size() > 0)
            System.out.println("Redirected to: " + redirectedTo.stream()
                    .map(services -> services.name().toLowerCase())
                    .collect(Collectors.joining(", ")));
    }
}
