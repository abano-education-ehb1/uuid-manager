package be.ehb.uuidmanager.rabbitmq;

import be.ehb.uuidmanager.config.settings.rabbitmq.RabbitMQCredentialsConfigPart;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class RabbitMQManager {

    @Getter
    static ConnectionFactory factory;
    @Getter
    static ConnectionFactory f;
    @Getter
    static Connection connection;
    @Getter
    static Channel channel;

    /**
     * Initialize RabbitMQ Connection
     * @param config The config file with RabbitMQ Credentials
     */
    @SneakyThrows
    public void init(@NonNull RabbitMQCredentialsConfigPart config){
        // Setup connection
        factory = new ConnectionFactory();
        factory.setHost(config.getHost());
        factory.setUsername(config.getUsername());
        factory.setPassword(config.getPassword());
        factory.setPort(config.getPort());

        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    /**
     * Send message via RabbitMQ
     * @param message The message to be sent
     */
    @SneakyThrows
    public void sendMessage(@NonNull String queueName, @NonNull String message){
        channel.queueDeclare(queueName, false, false, false, null);
        channel.basicPublish("", queueName, null, message.getBytes());
    }

    /**
     * Add a listener for any RabbitMQ message
     * @param consumer The listener
     */
    @SneakyThrows
    public static void addListener(@NonNull String channelName, @NonNull Consumer consumer){
        channel.queueDeclare(channelName, false, false, false, null);
        channel.basicConsume(channelName, true, consumer);
    }
}
