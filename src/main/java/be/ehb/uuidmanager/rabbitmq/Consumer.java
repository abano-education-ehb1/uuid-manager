package be.ehb.uuidmanager.rabbitmq;


import com.rabbitmq.client.ShutdownSignalException;

import java.io.IOException;

@FunctionalInterface
public interface Consumer extends com.rabbitmq.client.Consumer {

    @Override
    default void handleConsumeOk(String consumerTag){

    }

    @Override
    default void handleCancelOk(String consumerTag){

    }

    @Override
    default void handleCancel(String consumerTag) throws IOException {

    }

    @Override
    default void handleShutdownSignal(String consumerTag, ShutdownSignalException sig){

    }

    @Override
    default void handleRecoverOk(String consumerTag){

    }
}
