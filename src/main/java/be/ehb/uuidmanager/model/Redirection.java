package be.ehb.uuidmanager.model;

import be.ehb.uuidmanager.enums.Services;
import lombok.Data;

@Data
public class Redirection {

    private final Services service;
    private final String queueName;
}
