package be.ehb.uuidmanager.model.calendarlinks;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.EntityTypes;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import java.util.Objects;

@Data
@JsonRootName("calendarlink")
public class CalendarLinks implements Model<CalendarLinks> {

    @JsonProperty("source")
    String source = "";
    @JsonProperty("entity")
    String entity = "";
    @JsonProperty("userID")
    String userId;
    @JsonProperty("link-html")
    String linkHtml;
    @JsonProperty("link-ics")
    String linkIcs;
    @JsonIgnore
    String uuid;

    @Override
    public void apply() {
        val possibleEntity = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getUserId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();
        val possibleCompany = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getUserId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.COMPANY.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (Objects.isNull(possibleEntity) && Objects.isNull(possibleCompany))
            throw new RuntimeException("The " + EntityTypes.USER.getName() + " or " + EntityTypes.COMPANY.getName() + " from '" + getSource() + "' with id '" + getUserId() + "' is not found in the database");

        if(Objects.nonNull(possibleEntity))
            setUuid(possibleEntity.getUuid());
        else
            setUuid(possibleCompany.getUuid());
    }

    @SneakyThrows
    @Override
    public CalendarLinks convertToService(@NonNull Services service) {
        val model = (CalendarLinks) this.clone();
        val dbEntity = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();
        val dbCompany = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.COMPANY.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();

        if (Objects.isNull(dbEntity) && Objects.isNull(dbCompany))
            throw new RuntimeException("No " + EntityTypes.USER.getName() + " or " + EntityTypes.COMPANY.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getUuid() + "'. Cannot redirect to " + service.getName() + ".");

        if(Objects.nonNull(dbEntity))
            model.setUserId(dbEntity.getEntityId());
        else
            model.setUserId(dbCompany.getEntityId());

        return model;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "'";
    }
}
