package be.ehb.uuidmanager.model.order;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.EntityTypes;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import java.util.Objects;

@Data
@JsonRootName("order")
public class Order implements Model<Order> {

    @JsonProperty("source")
    String source = "";
    @JsonProperty("source-id")
    String sourceId = "";
    @JsonProperty("action")
    String action = "";
    @JsonProperty("uuid")
    String uuid = "";

    @JsonProperty("properties")
    OrderProperties properties = new OrderProperties();

    @Override
    public void apply() {
        val possibleUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getProperties().getUser().getSourceId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (Objects.isNull(possibleUser))
            throw new RuntimeException("The " + EntityTypes.USER.getName() + " from '" + getSource() + "' with " + EntityTypes.USER.getName() + " id '" + getProperties().getUser().getSourceId() + "' is not found in the database");

        getProperties().getUser().setUuid(possibleUser.getUuid());

        val possibleEntity = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getSourceId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.ORDER.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (getAction().equals("create")) {
            if (Objects.nonNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.ORDER.getName() + " from '" + getSource() + "' with " + EntityTypes.ORDER.getName() + " id '" + getSourceId() + "' already exist in the database");

            val dbEntity = new Entities();
            dbEntity.setEntityId(getSourceId());
            dbEntity.setSystem(getSource());
            dbEntity.setEntityVersion(1);
            dbEntity.setEntityType(EntityTypes.ORDER.getName());
            setUuid(DataManager.getEntityUUIDStore().insert(dbEntity).getUuid());
        } else if (getAction().equals("update")) {
            if (Objects.isNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.ORDER.getName() + " from '" + getSource() + "' with " + EntityTypes.ORDER.getName() + " id '" + getSourceId() + "' is not found in the database");

            possibleEntity.setEntityVersion(possibleEntity.getEntityVersion() + 1);
            DataManager.getEntityUUIDStore().update(possibleEntity);
            setUuid(possibleEntity.getUuid());
        } else if (getAction().equals("delete")) {
            if (Objects.isNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.ORDER.getName() + " from '" + getSource() + "' with " + EntityTypes.ORDER.getName() + " id '" + getSourceId() + "' is not found in the database");

            DataManager.getEntityUUIDStore().delete(possibleEntity);
            setUuid(possibleEntity.getUuid());
        }
    }

    @SneakyThrows
    @Override
    public Order convertToService(@NonNull Services service) {
        val model = (Order) this.clone();
        if (!getAction().equals("create")) {
            val dbEntity = DataManager.getEntityUUIDStore()
                    .select(Entities.class)
                    .where(Entities.UUID.eq(getUuid()))
                    .and(Entities.ENTITY_TYPE.eq(EntityTypes.ORDER.getName()))
                    .and(Entities.SYSTEM.eq(service.getName()))
                    .get().firstOrNull();

            if (Objects.isNull(dbEntity))
                throw new RuntimeException("No " + EntityTypes.ORDER.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getUuid() + "'. Cannot redirect to " + service.getName() + ".");

            model.setSourceId(dbEntity.getEntityId());
        }

        val dbUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getProperties().getUser().getUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();

        if (Objects.isNull(dbUser))
            throw new RuntimeException("No " + EntityTypes.USER.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getProperties().getUser().getUuid() + "'. Cannot redirect to " + service.getName() + ".");

        model.getProperties().getUser().setSourceId(dbUser.getEntityId());
        return model;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "' with action '" + getAction() + "'";
    }
}
