package be.ehb.uuidmanager.model.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrderProperties {

    @JsonProperty("date")
    long date = 0;
    @JsonProperty("total")
    double total = 0.0;
    @JsonProperty("user")
    UserProperty user = new UserProperty();
    @JsonProperty("invoice")
    boolean invoice;
    @JacksonXmlElementWrapper(localName = "lines")
    @JacksonXmlProperty(localName = "line")
    List<Object> line = new ArrayList<>();
    @JsonProperty("is-paid")
    boolean isPaid;
}
