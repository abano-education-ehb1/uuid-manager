package be.ehb.uuidmanager.model.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserProperty {

    @JsonProperty("source-id")
    String sourceId = "";
    @JsonProperty("uuid")
    String uuid = "";
}
