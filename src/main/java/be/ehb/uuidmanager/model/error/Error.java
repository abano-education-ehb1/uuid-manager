package be.ehb.uuidmanager.model.error;

import be.ehb.uuidmanager.rabbitmq.RabbitMQManager;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@JsonRootName("error")
public class Error {

    final String source = "uuid_manager";
    final long date = new Date().getTime();
    final String level;
    final String message;

    public Error(Level level, String message) {
        this.level = level.getName().toLowerCase();
        this.message = message;
    }

    @Getter
    @AllArgsConstructor
    public enum Level{
        WARNING("warn"),
        ERROR("error"),
        FATAL("fatal");

        private final String name;
    }

    @SneakyThrows
    public void send(){
        RabbitMQManager.sendMessage("errors", new XmlMapper().writeValueAsString(this));
    }
}
