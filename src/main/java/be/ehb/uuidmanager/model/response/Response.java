package be.ehb.uuidmanager.model.response;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import lombok.val;

import java.util.Objects;

@Data
@Log4j2
@JsonRootName("response")
public class Response implements Model<Response> {

    String source = "";
    @JsonProperty("source-id")
    String sourceId = "";
    String uuid;
    String entity;
    String action;

    @Override
    public void apply() {
        val PossibleEntity = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUuid()))
                .and(Entities.ENTITY_TYPE.eq(getEntity()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (getAction().equalsIgnoreCase("created")) {
            if (Objects.nonNull(PossibleEntity))
                throw new RuntimeException("The " + getEntity() + " from '" + getSource() + "' with uuid '" + getUuid() + "' already exist in the database");

            val dbEntity = new Entities();
            dbEntity.setUuid(getUuid());
            dbEntity.setEntityId(getSourceId());
            dbEntity.setSystem(getSource());
            dbEntity.setEntityVersion(1);
            dbEntity.setEntityType(getEntity());
            DataManager.getEntityUUIDStore().insert(dbEntity);
        } else if (getAction().equalsIgnoreCase("updated")) {
            if (Objects.isNull(PossibleEntity))
                throw new RuntimeException("The " + getEntity() +" from '" + getSource() + "' with uuid '" + getUuid() + "' is not found in the database");

            PossibleEntity.setEntityVersion(PossibleEntity.getEntityVersion() + 1);
            DataManager.getEntityUUIDStore().update(PossibleEntity);
        } else if (getAction().equalsIgnoreCase("deleted")) {
            if (Objects.isNull(PossibleEntity))
                throw new RuntimeException("The " + getEntity() + " from '" + getSource() + "' with uuid '" + getUuid() + "' is not found in the database");

            DataManager.getEntityUUIDStore().delete(PossibleEntity);
        }
    }

    @Override
    public Response convertToService(@NonNull Services service) {
        // Nothing to do
        return null;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "' with action '" + getAction() + "'";
    }
}
