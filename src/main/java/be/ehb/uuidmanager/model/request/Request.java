package be.ehb.uuidmanager.model.request;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.EntityTypes;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.model.order.Order;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import java.util.Objects;

@Data
@JsonRootName("request")
public class Request implements Model<Request> {

    @JsonProperty("source")
    String source = "";
    @JsonProperty("source-id")
    String sourceId = "";
    @JsonProperty("uuid")
    String uuid = "";
    @JsonProperty("action")
    String action = "";

    @Override
    public void apply() {
        val possibleUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getSourceId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (Objects.isNull(possibleUser))
            throw new RuntimeException("The " + EntityTypes.USER.getName() + " from '" + getSource() + "' with " + EntityTypes.USER.getName() + " id '" + getSourceId() + "' is not found in the database");

        setUuid(possibleUser.getUuid());
    }

    @SneakyThrows
    @Override
    public Request convertToService(@NonNull Services service) {
        val model = (Request) this.clone();
        val dbUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();

        if (Objects.isNull(dbUser))
            throw new RuntimeException("No " + EntityTypes.USER.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getUuid() + "'. Cannot redirect to " + service.getName() + ".");

        model.setSourceId(dbUser.getEntityId());
        return model;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "' with action '" + getAction() + "'";
    }
}
