package be.ehb.uuidmanager.model.usersession;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.EntityTypes;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import java.util.Objects;

@Data
@JsonRootName("user-session")
public class UserSession implements Model<UserSession> {

    @JsonProperty("source")
    String source = "";
    @JsonProperty("sessionId")
    String sessionId = "";
    @JsonProperty("sessionName")
    String sessionName = "";
    @JsonProperty("userId")
    String userId = "";
    @JsonProperty("action")
    String action = "";
    @JsonProperty("email")
    String email = "";

    @JsonIgnore
    String userUuid = "";
    @JsonIgnore
    String sessionUuid = "";

    @Override
    public void apply() {
        val possibleUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getUserId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();
        val possibleCompany = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getUserId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.COMPANY.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (Objects.isNull(possibleUser) && Objects.isNull(possibleCompany))
            throw new RuntimeException("The " + EntityTypes.USER.getName() + " or " + EntityTypes.COMPANY.getName() + " from '" + getSource() + "' with " + EntityTypes.USER.getName() + " id '" + getUserId() + "' is not found in the database");


        if(Objects.nonNull(possibleUser))
            setUserUuid(possibleUser.getUuid());
        else
            setUserUuid(possibleCompany.getUuid());

        val possibleSession = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getSessionId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.SESSION.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (Objects.isNull(possibleSession))
            throw new RuntimeException("The " + EntityTypes.SESSION.getName() + " from '" + getSource() + "' with " + EntityTypes.SESSION.getName() + " id '" + getSessionId() + "' is not found in the database");

        setSessionUuid(possibleSession.getUuid());
    }

    @SneakyThrows
    @Override
    public UserSession convertToService(@NonNull Services service) {
        val model = (UserSession) this.clone();

        val dbUser = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUserUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.USER.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();
        val dbCompany = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getUserUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.COMPANY.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();

        if (Objects.isNull(dbUser) && Objects.isNull(dbCompany))
            throw new RuntimeException("No " + EntityTypes.USER.getName() + " or " + EntityTypes.COMPANY.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getUserUuid() + "'. Cannot redirect to " + service.getName() + ".");

        if(Objects.nonNull(dbUser))
            model.setUserId(dbUser.getEntityId());
        else
            model.setUserId(dbCompany.getEntityId());

        val dbSession = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.UUID.eq(getSessionUuid()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.SESSION.getName()))
                .and(Entities.SYSTEM.eq(service.getName()))
                .get().firstOrNull();

        if(!service.equals(Services.MAILING)) {
            if (Objects.isNull(dbSession))
                throw new RuntimeException("No " + EntityTypes.SESSION.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getSessionUuid() + "'. Cannot redirect to " + service.getName() + ".");

            model.setSessionId(dbSession.getEntityId());
        }

        return model;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "'";
    }
}
