package be.ehb.uuidmanager.model.session;

import be.ehb.uuidmanager.data.DataManager;
import be.ehb.uuidmanager.data.tables.Entities;
import be.ehb.uuidmanager.enums.EntityTypes;
import be.ehb.uuidmanager.enums.Services;
import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.utils.ArgsUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Data
@JsonRootName("session")
public class Session implements Model<Session> {

    @JsonProperty("source")
    String source = "";
    @JsonProperty("action")
    String action = "";
    @JsonProperty("source-id")
    String sourceId = "";
    @JsonProperty("uuid")
    String uuid = "";

    @JsonProperty("properties")
    Map<String, Object> properties = new HashMap<>();

    @Override
    public void apply() {
        val possibleEntity = DataManager.getEntityUUIDStore()
                .select(Entities.class)
                .where(Entities.ENTITY_ID.eq(getSourceId()))
                .and(Entities.ENTITY_TYPE.eq(EntityTypes.SESSION.getName()))
                .and(Entities.SYSTEM.eq(getSource()))
                .get().firstOrNull();

        if (getAction().equals("create")) {
            if (Objects.nonNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.SESSION.getName() +  " from '" + getSource() + "' with id '" + getSourceId() + "' already exist in the database");

            val dbEntity = new Entities();
            dbEntity.setEntityId(getSourceId());
            dbEntity.setSystem(getSource());
            dbEntity.setEntityVersion(1);
            dbEntity.setEntityType(EntityTypes.SESSION.getName());
            setUuid(DataManager.getEntityUUIDStore().insert(dbEntity).getUuid());
        } else if (getAction().equals("update")) {
            if (Objects.isNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.SESSION.getName() + " from '" + getSource() + "' with id '" + getSourceId() + "' is not found in the database");

            possibleEntity.setEntityVersion(possibleEntity.getEntityVersion() + 1);
            DataManager.getEntityUUIDStore().update(possibleEntity);
            setUuid(possibleEntity.getUuid());
        } else if (getAction().equals("delete")) {
            if (Objects.isNull(possibleEntity))
                throw new RuntimeException("The " + EntityTypes.SESSION.getName() + " from '" + getSource() + "' with id '" + getSourceId() + "' is not found in the database");

            DataManager.getEntityUUIDStore().delete(possibleEntity);
            setUuid(possibleEntity.getUuid());
        }
    }

    @SneakyThrows
    @Override
    public Session convertToService(@NonNull Services service) {
        val model = (Session) this.clone();
        if (!getAction().equals("create")) {
            val dbEntity = DataManager.getEntityUUIDStore()
                    .select(Entities.class)
                    .where(Entities.UUID.eq(getUuid()))
                    .and(Entities.ENTITY_TYPE.eq(EntityTypes.SESSION.getName()))
                    .and(Entities.SYSTEM.eq(service.getName()))
                    .get().firstOrNull();

            if (Objects.isNull(dbEntity))
                throw new RuntimeException("No " + EntityTypes.SESSION.getName() + " found in '" + service.getName() + "' that is linked to the uuid '" + getUuid() + "'. Cannot redirect to " + service.getName() + ".");

            model.setSourceId(dbEntity.getEntityId());
        }
        return model;
    }

    @Override
    public String retrieveInfo() {
        return "Received '" + ArgsUtils.convert(getClass().getSimpleName()) + "' entity from '" + getSource() + "' with action '" + getAction() + "'";
    }
}
