package be.ehb.uuidmanager.model;

import be.ehb.uuidmanager.enums.Services;
import lombok.NonNull;

public interface Model<T extends Model<?>> extends Cloneable {

    String getSource();
    void apply();
    T convertToService(@NonNull Services service);
    String retrieveInfo();
}
