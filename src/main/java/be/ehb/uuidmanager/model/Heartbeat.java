package be.ehb.uuidmanager.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.ToString;

import java.time.Instant;
import java.util.Date;

@lombok.Data
@ToString
@JsonRootName("heartbeat")
public class Heartbeat {

    final String source = "uuid_manager";
    final long date = Instant.now().getEpochSecond();
}
