package be.ehb.uuidmanager.enums;

import be.ehb.uuidmanager.model.Model;
import be.ehb.uuidmanager.model.Redirection;
import be.ehb.uuidmanager.model.calendarlinks.CalendarLinks;
import be.ehb.uuidmanager.model.company.Company;
import be.ehb.uuidmanager.model.order.Order;
import be.ehb.uuidmanager.model.request.Request;
import be.ehb.uuidmanager.model.response.Response;
import be.ehb.uuidmanager.model.session.Session;
import be.ehb.uuidmanager.model.user.User;
import be.ehb.uuidmanager.model.usersession.UserSession;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.val;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public enum Entities {
    USER(User.class, "xsd/user.xsd",
            new Redirection(Services.MONITORING, "monitoring"),
            new Redirection(Services.FACTURATIE, "facturatie"),
            new Redirection(Services.CRM, "crm"),
            new Redirection(Services.PLANNING, "planning_user_queue"),
            new Redirection(Services.FRONTEND, "frontend"),
            new Redirection(Services.KASSA, "kassa"),
            new Redirection(Services.MAILING, "mailing")),
    SESSION(Session.class, "xsd/session.xsd",
            new Redirection(Services.CRM, "crm"),
            new Redirection(Services.PLANNING, "planning_session_queue"),
            new Redirection(Services.FRONTEND, "frontend")),
    RESPONSE(Response.class, "xsd/response.xsd"),
    ORDER(Order.class, "xsd/order.xsd",
            new Redirection(Services.FACTURATIE, "facturatie")/*,
            new Redirection(Services.KASSA, "kassa")*/),
    COMPANY(Company.class, "xsd/company.xsd",
            new Redirection(Services.FACTURATIE, "facturatie"),
            new Redirection(Services.CRM, "crm"),
            new Redirection(Services.MAILING, "mailing"),
            new Redirection(Services.KASSA, "kassa"),
            new Redirection(Services.PLANNING, "planning_user_queue"),
            new Redirection(Services.FRONTEND, "frontend")),
    CALENDAR_LINKS(CalendarLinks.class, "xsd/calendarLink.xsd",
            /*new Redirection(Services.PLANNING, "planning"),*/
            new Redirection(Services.FRONTEND, "frontend")),
    USER_SESSION(UserSession.class, "xsd/userSession.xsd",
            new Redirection(Services.CRM, "crm"),
            new Redirection(Services.PLANNING, "planning_session_queue"),
            new Redirection(Services.MAILING, "mailing")),
    REQUEST(Request.class, "xsd/request.xsd",
            new Redirection(Services.FACTURATIE, "facturatie"));

    @Getter
    private final Class<? extends Model<?>> clazz;
    @Getter
    private final List<Redirection> redirections;
    private Schema xsdSchema = null;

    Entities(@NonNull Class<? extends Model<?>> clazz, @NonNull String xsdPath, Redirection... redirections) {
        this.clazz = clazz;
        this.redirections = Arrays.stream(redirections).toList();

        // Get schema factory instance
        val schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try (val inputStream = this.getClass().getClassLoader().getResourceAsStream(xsdPath);
             val streamReader = new InputStreamReader(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);
             val reader = new BufferedReader(streamReader)) {
            val builder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            xsdSchema = schemaFactory.newSchema(new StreamSource(new StringReader(builder.toString())));
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    public boolean isValid(@NonNull String xml) {
        try {
            // Create validator from schema
            val validator = xsdSchema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
            return true;
        } catch (SAXException | IOException ignored) {
        }
        return false;
    }

    public Model<?> convert(@NonNull String xml){
        try {
            return new XmlMapper().readValue(xml, clazz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            // TODO Error log
        }
        return null;
    }

    public static Entities convertToEntity(@NonNull String xml){
        return Arrays.stream(Entities.values())
                .filter(entities -> entities.isValid(xml))
                .findFirst().orElse(null);
    }
}
