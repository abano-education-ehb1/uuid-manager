package be.ehb.uuidmanager.enums;

import lombok.Getter;

@Getter
public enum EntityTypes {
    USER("user"),
    SESSION("session"),
    ORDER("order"),
    COMPANY("company");

    private final String name;

    EntityTypes(String name) {
        this.name = name;
    }
}
