package be.ehb.uuidmanager.enums;

import lombok.Getter;

@Getter
public enum Services {
    MONITORING,
    FACTURATIE,
    CRM,
    PLANNING,
    FRONTEND,
    KASSA,
    MAILING;

    private final String name;

    Services() {
        this.name = name().toLowerCase();
    }
}
