package be.ehb.uuidmanager.tasks;

import be.ehb.uuidmanager.model.Heartbeat;
import be.ehb.uuidmanager.model.error.Error;
import be.ehb.uuidmanager.rabbitmq.RabbitMQManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.log4j.Log4j2;

import java.util.TimerTask;

@Log4j2
public class HeartbeatTask extends TimerTask {
    @Override
    public void run() {
        try {
            RabbitMQManager.sendMessage("monitoring", new XmlMapper().writeValueAsString(new Heartbeat()));
        } catch (JsonProcessingException e) {
            log.error("An error occurs while sending the heartbeat.", e);
        }
    }
}
