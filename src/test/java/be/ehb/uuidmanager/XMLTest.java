package be.ehb.uuidmanager;

import be.ehb.uuidmanager.model.calendarlinks.CalendarLinks;
import be.ehb.uuidmanager.model.company.Company;
import be.ehb.uuidmanager.model.order.Order;
import be.ehb.uuidmanager.model.response.Response;
import be.ehb.uuidmanager.model.session.Session;
import be.ehb.uuidmanager.model.user.User;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class XMLTest {

    @SneakyThrows
    public static <T> T convertToObjet(@NonNull String xml, Class<T> clazz) {
        return new XmlMapper().readValue(xml, clazz);
    }

    @Test
    public void calendarLinks() {
        val o = convertToObjet("""
                <calendarlink>
                    <source>planning</source>
                    <entity>calendar</entity>
                    <userID>id</userID>
                    <link-html>http://google.com</link-html>
                    <link-ics>http://ehb.be</link-ics>
                </calendarlink>
                """, CalendarLinks.class);
        assertEquals("calendar", o.getEntity());
    }

    @Test
    public void company() {
        val o = convertToObjet("""
                <company>
                  <source>string</source>
                  <source-id>string</source-id>
                  <uuid>string</uuid>
                  <action>update</action>
                  <properties>
                    <name>string</name>
                    <email>string</email>
                    <address>
                      <street>string</street>
                      <housenumber>4635</housenumber>
                      <postalcode>1802</postalcode>
                      <city>string</city>
                      <country>string</country>
                    </address>
                    <phone>string</phone>
                    <taxId>string</taxId>
                  </properties>
                </company>
                """, Company.class);
        assertEquals("update", o.getAction());
    }

    @Test
    public void order() {
        val o = convertToObjet("""
                <order>
                    <source>kassa</source>
                    <source-id>55</source-id>
                    <uuid>string</uuid>
                    <action>create</action>
                    <properties>
                      <date>16000000</date>
                      <total>6.00</total>
                      <user>
                          <source-id>55</source-id>
                          <uuid>66</uuid>
                      </user>
                      <invoice>true</invoice>
                      <lines>
                        <line>
                          <product-name>cola</product-name>
                          <unity-price>2.00</unity-price>
                          <quantity>2</quantity>
                        </line>
                        <line>
                          <product-name>chips</product-name>
                          <unity-price>1.50</unity-price>
                          <quantity>1</quantity>
                        </line>
                      </lines>
                      <is-paid>true</is-paid>
                    </properties>
                </order>
                """, Order.class);
        assertEquals("create", o.getAction());
    }

    @Test
    public void response() {
        val o = convertToObjet("""
                <response>
                    <source>kassa</source>
                    <source-id>55</source-id>
                    <uuid>2ccf2a11-9cb0-42fe-9d5f-0e4d21f8c9e5</uuid>
                    <entity>user</entity>
                    <action>created</action>
                  </response>
                """, Response.class);
        assertEquals("created", o.getAction());
    }

    @Test
    public void session() {
        val o = convertToObjet("""
                <session>
                  <source>frontend</source>
                  <action>create</action>
                  <source-id>5</source-id>
                  <uuid>5d0239a4-d14e-11ec-9d64-0242ac120002</uuid>
                  <properties>
                    <name>Test Session</name>
                    <beginDateTime>2022-04-12 16:00:00</beginDateTime>
                    <endDateTime>2022-04-12 18:00:00</endDateTime>
                    <speaker>Bob De bouwer</speaker>
                    <location>A1</location>
                    <description>Event description.</description>
                  </properties>
                </session>
                """, Session.class);
        assertEquals("create", o.getAction());
    }

    @Test
    public void user() {
        val o = convertToObjet("""
                <user>
                  <source>frontend</source>
                  <source-id>string</source-id>
                  <uuid>string</uuid>
                  <action>update</action>
                  <properties>
                    <firstname>string</firstname>
                    <lastname>string</lastname>
                    <email>string</email>
                    <address>
                      <street>string</street>
                      <housenumber>4635</housenumber>
                      <postalcode>1802</postalcode>
                      <city>string</city>
                      <country>string</country>
                    </address>
                    <phone>string</phone>
                    <company>
                      <source-id></source-id>
                      <uuid></uuid>
                    </company>
                  </properties>
                </user>
                """, User.class);
        assertEquals("update", o.getAction());
    }
}
